#include "ObjectiveNode.h"

int main()
{
    Node *test = new Node(10);
    top = test;

    Node *test2 = new Node(20);
    top->insertAtTheEnd(test2);

    Node *test3 = new Node(30);
    top->insertAtTheEnd(test3);

    Node *test4 = new Node(40);
    top->insertAtTheEnd(test4);

    Node *testCount;
    testCount = top->getLastNode();
    testCount->getNodeCount();

    Node *testRearNode;
    testRearNode = testCount->getRearNode();
    testRearNode->printCurrentNodeData();
    testRearNode->getNodeCount();
    //testCount->deleteNode();

    Node *testMidDelete;
    testMidDelete = top->getNextNode();
    testMidDelete->printCurrentNodeData();
    Node *getTopNode;
    getTopNode = testMidDelete->getRearNode();
    getTopNode->printCurrentNodeData();

    //testMidDelete->deleteNode();
    top->deleteNode();
    //testCount->deleteNode();

    //testMidDelete->deleteNode();
    top->printTillEnd();

    return 0;
}


Node::Node(int data)
{
    this->data = data;
    this->next = NULL;
    this->rear = NULL;
}

Node *Node::getLastNode()
{
    Node *temp;
    temp = this;
    while(temp->next != NULL)
    {
        temp = temp->next;
    }
    return temp;
}

Node *Node::getNextNode()
{
    Node *temp;
    temp = this->next;
    return temp;
}

Node *Node::getRearNode()
{
    Node *temp;
    temp = this->rear;
    return temp;
}

int Node::getNodeCount()
{
    Node *temp;
    temp = top;
    int nodeCount = 1;
    if(temp == this)
        cout<<"Count: " << nodeCount << "\n";
    else
    {
        while(temp->next != this)
        {
            temp = temp->next;
            nodeCount++;
        }
        nodeCount = nodeCount + 1;
        cout<<"Count: " << nodeCount << "\n";
    }
}

void Node::appendNode(Node *newNode)
{
    //funcation to add new node at the end of calling node.
    Node *thisNode;
    thisNode = this;
    if(this->next == NULL)
    {
        //cout << "Calling node: " << this->data << "\n";
        this->next = newNode;
        newNode->next = NULL;
        newNode->rear = thisNode;
        //cout << "Inserted node: " << newNode->data << "\n";
    }
    else
        cout<<"Cannot append in middle. ";
}

void Node::insertAtTheEnd(Node *newNode)
{
    Node *temp;
    temp = top->getLastNode();
    //cout << "Last value: " << temp->data << "\n";
    //cout << "to be inserted value: " << newNode->data << "\n";
    temp->appendNode(newNode);
}

void Node::deleteNode()
{
    Node *toDelete;
    //cout << "to be deleted node: " << this->data << "\n";
    if (this->rear == NULL && this->next != NULL)
    {
        toDelete = this;
        top = this->next;
        top->rear = NULL;
        delete toDelete;
    }
    else if (this->next != NULL && this->rear != NULL)
    {
        toDelete = this;
        this->rear->next = this->next;
        this->next->rear = this->rear;
        delete toDelete;
    }
    else if (this->rear != NULL && this->next == NULL)
    {
        toDelete = this;
        this->rear->next = NULL;
        delete toDelete;
    }
    else
        cout<< "Nothing to do. \n";

    return;
}

void Node::printCurrentNodeData()
{
    cout<<"Data: " << this->data << "\n";
    std::this_thread::sleep_for (std::chrono::seconds(1));
}

void Node::printTillEnd()
{
    Node *temp;
    temp = this;
    while(temp->next != NULL)
    {
        temp->printCurrentNodeData();
        temp = temp->next;
    }
    temp->printCurrentNodeData();
}
