#ifndef OBJECTIVENODE_H
#define OBJECTIVENODE_H

#include <iostream>
#include <thread>
#include <chrono>
using namespace std;

class Node
{
private:
    int data;
    class Node *next;
    class Node *rear;

public:
    Node(int data);

    Node *getLastNode();
    Node *getNextNode();
    Node *getRearNode();
    int getNodeCount();

    void appendNode(Node *newNode);
    void insertAtTheEnd(Node *newNode);
    void deleteNode();

    void printCurrentNodeData();
    void printTillEnd();
} *top;

#endif // OBJECTIVENODE_H
